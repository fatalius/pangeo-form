import {Ref, ref, watchEffect} from 'vue'
import {DataItem} from '@/components/PangeoForm/use/useDataGenerator'

export interface PaginatorOptions {
  itemsOnPage: number,
  sortBy: string,
  sortDirection: number
}

interface UsePaginator {
  page: Ref<number>,
  items: Ref<DataItem[]>,
  totalPages: Ref<number>,
  hasPrevPage: Ref<boolean>,
  hasNextPage: Ref<boolean>,
}

export function usePaginator(paginatorOptions: PaginatorOptions, data: Ref<DataItem[]>) : UsePaginator {
  const page = ref(1)
  const items = ref<DataItem[]>([])
  const hasNextPage = ref(false)
  const hasPrevPage = ref(false)
  const totalPages = ref(0)

  function filterItems() {
    totalPages.value = paginatorOptions.itemsOnPage > 0 ? Math.ceil(data.value.length / paginatorOptions.itemsOnPage) : 1

    hasNextPage.value = page.value !== totalPages.value
    hasPrevPage.value = page.value !== 1

    if (page.value < 1) page.value = 1
    if (page.value > totalPages.value) page.value = totalPages.value

    const startIndex = (page.value - 1) * paginatorOptions.itemsOnPage
    const endIndex = startIndex + paginatorOptions.itemsOnPage
    const sortKey = paginatorOptions.sortBy??'id'

    const unrefData = Array.from(data.value)

    unrefData.sort((a,b) => {
      const [aValue, bValue] = [a[sortKey as keyof DataItem], b[sortKey as keyof DataItem]]
      return (aValue > bValue ? 1 : -1) * paginatorOptions.sortDirection
    })

    items.value = unrefData.filter((dataItem, index) => index >= startIndex && index < endIndex)
  }

  watchEffect(filterItems)

  return {
    page,
    items,
    totalPages,
    hasPrevPage,
    hasNextPage
  }
}
