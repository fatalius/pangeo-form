import {Ref, ref, watchEffect } from 'vue'

export interface DataProviderOptions {
  recordsCount: number,
  recordsValues: string[]
}

export interface DataItem {
  id: number,
  title: string,
  rand: string
}

interface GeneratorData {
  items: Ref<DataItem[]>
}

export function useDataGenerator(dataProviderOptions: DataProviderOptions) : GeneratorData  {
  const items = ref<DataItem[]>([])

  function updateItems() {
    items.value = []

    for (let id = 1; id <= dataProviderOptions.recordsCount; id++) {
      const valuesList: string[] = dataProviderOptions.recordsValues
      const randomIndex = Math.round(Math.random() * (valuesList.length - 1))

      items.value.push({
        id,
        title: `name${id}`,
        rand: valuesList[randomIndex]
      })
    }
  }

  watchEffect(updateItems)

  return {items}
}
