import {ref, Ref } from 'vue'

export interface FormField {
  id: string,
  placeholder: string,
  model: string | number,
  type: string,
  hasError: boolean
}

interface Validators {
  number: (value: string | number) => boolean,
  records: (value: string | number) => boolean,
}

const numValidator = (value: string | number) : boolean => {
  return isNaN(Number(value)) || Number(value) === 0 || String(value).trim() === ''
}
const recordsValidator = (value: string | number) : boolean => {
  return String(value).split(',').filter((value) => value.trim() !== '').length === 0
}

const validators : Validators  = {
  number: numValidator,
  records: recordsValidator
}

interface UseForm {
  formHasAnyError: Ref<boolean>
  validateForm: () => void
}

export function useForm(formFields : Ref<FormField[]>) : UseForm {
  const formHasAnyError = ref(false)

  const validateForm = () => {
    formHasAnyError.value = false

    formFields.value.forEach((formField) => {
      const validator = validators[formField.type as keyof Validators]
      formField.hasError = validator(formField.model)
      if (formField.hasError) formHasAnyError.value = true
    })
  }

  return {
    formHasAnyError,
    validateForm
  }
}
