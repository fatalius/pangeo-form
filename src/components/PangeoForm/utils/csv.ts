export function downloadCSV(data: string[][]) : void {
  const csv = data.map(row => row
    .map(String)
    .map(v => v.replaceAll('"', '""'))
    .map(v => `"${v}"`)
    .join(',')
  ).join('\r\n')

  const blobFile = new Blob([csv], { type: 'text/csv;charset=utf-8;' })
  const url = URL.createObjectURL(blobFile)
  const link = document.createElement('a')
  link.href = url
  link.setAttribute('download', 'export.csv')
  link.click()
}
